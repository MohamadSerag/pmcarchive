import sys
from os import chdir
from cx_Freeze import setup, Executable

chdir('./app')
sys.path.append('.')
base = 'Win32GUI'
resources = ['res']
options = {'build_exe': {'include_files': resources}}

executables = [Executable('main.py', base=base, icon='../img/app.ico',  targetName='PMCArchive.exe')]
setup(options=options, executables=executables)
