from argparse import ArgumentParser


def parse_args():
    par = ArgumentParser()
    par.add_argument('cmd')
    par.add_argument('-m', '--mode', dest='mode', default='default')
    return par.parse_args()


def manage_tables(args):
    from peewee import SqliteDatabase
    from app import models
    from app.utils import get_orm_classes

    models.orm_db_conn.initialize(SqliteDatabase('app/res/app-dev.db'))
    for cls in get_orm_classes(models):
        if args.cmd == 'initdb':
            cls.create_table(safe=True)
            print(cls._meta.table_name, 'created..')
        elif args.cmd == 'deinitdb':
            cls.drop_table()
            print(cls._meta.table_name, 'deleted..')

    print('done.')


def insert_initial_pmc_files(args):
    import csv
    from os import listdir
    from app import models
    from peewee import SqliteDatabase
    from app.models import PMCFile
    
    models.orm_db_conn.initialize(SqliteDatabase('app/res/app-dev.db'))

    for filename in listdir('initialdata/xlfiles'):
        with open('initialdata/xlfiles/'+filename, 'rb') as file:
            f = PMCFile(val=file.read())
            f.save(force_insert=True)


def uic(args):
    import os
    import subprocess as sub

    sub.call(['pyrcc5', 'img/icons.qrc', '-o', 'app/icons_rc.py'])
    sub.call(['pyuic5', '-x', 'app/views/mainview.ui', '-o', 'app/views/mainview.py'])
    sub.call(['pyuic5', '-x', 'app/views/pmceditorview.ui', '-o', 'app/views/pmceditorview.py'])


def bld(args):
    from subprocess import call
    call(['python', '_bld.py', 'build', '-b', '../'], shell=True)


def main():
    cmd_map = {
        'initdb': manage_tables,
        'deinitdb': manage_tables,
        'uic': uic,
        'initpmcfiles': insert_initial_pmc_files,
        'bld': bld
    }

    args = parse_args()
    cmd_map[args.cmd](args)


if __name__ == '__main__':
    main()
