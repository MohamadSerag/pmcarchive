import sys

from PyQt5.QtWidgets import QApplication
from peewee import SqliteDatabase

import models
from controllers import MainCtrl
from views import MainView
from utils import get_orm_classes, cfg


cfg.init(app_name='PMCArchive')
app = QApplication(sys.argv)

models.qt_db_conn = models.open_db_qconn('res/app-dev.db')
models.orm_db_conn.initialize(SqliteDatabase('res/app-dev.db'))

ps_tbl_omodel = [cls for cls in sorted(get_orm_classes(module=models, ps_cmbox_only=True), key=lambda cls: cls.display_order)]
cmbox_qmodels = [models.make_cmbox_model(omodel) for omodel in ps_tbl_omodel]
ps_tbl_header = [(cls._meta.table_name, cls.fk_name) for cls in ps_tbl_omodel]

relations = [
    {'tbl_name': 'Ratings', 'fk_colx': 2, 'fk_col_name': 'id', 'target_col_name': 'val', 'fk_col_header_label': 'rating'},
    {'tbl_name': 'Facings', 'fk_colx': 3, 'fk_col_name': 'id', 'target_col_name': 'val', 'fk_col_header_label': 'facing'},
    {'tbl_name': 'Materials', 'fk_colx': 4, 'fk_col_name': 'id', 'target_col_name': 'val', 'fk_col_header_label': 'material'},
    {'tbl_name': 'VTrims', 'fk_colx': 5, 'fk_col_name': 'id', 'target_col_name': 'val', 'fk_col_header_label': 'vtrim'},
    {'tbl_name': 'Companies', 'fk_colx': 6, 'fk_col_name': 'id', 'target_col_name': 'name', 'fk_col_header_label': 'company'},
]

pmc_tbl_qmodel = models.make_relational_model(
            models.PMC._meta.table_name, relations=relations, clause=MainCtrl.EMPTY_PMC_TBL_CLAUSE
        )
main_view = MainView(ps_tbl_header, cmbox_qmodels, pmc_tbl_qmodel)
cfg.handle_views([main_view])
main_ctrl = MainCtrl(main_view, pmc_tbl_qmodel)

main_view.show()
app.exec_()
cfg.save()

