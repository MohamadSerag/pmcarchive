from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt

from utils import cfg
from views import mainview


class MainView(QMainWindow, mainview.Ui_MainWindow):
    PS_TABLE_ROW_COUNT = 2
    CMBOX_ROWX = 0
    CBOX_ROWX = 1
    FK_NAME_INDEX = 1
    PMC_HIDDEN_COLX = {'id': 0, 'file_id': 7}

    def __init__(self, ps_tbl_header, ps_tbl_cmbox_qmodels, pmc_tbl_qmodel):
        super().__init__()
        self.context_menu = QMenu()
        self._setup(ps_tbl_cmbox_qmodels, ps_tbl_header, pmc_tbl_qmodel)

    def _add_logo(self, imgpath='res/enppi.png', maxsize=(60, 20)):
        self.statusbar.addWidget(QLabel(''))
        self._enppi_label = QLabel()
        self._enppi_label.setPixmap(QPixmap(imgpath))
        self._enppi_label.setScaledContents(True)
        self._enppi_label.setMaximumSize(*maxsize)
        self.statusbar.addWidget(self._enppi_label)

    def _add_toolbar(self):
        self.toolbar = QToolBar('Main')
        for action in sorted(
                [act for act in self.findChildren(QAction) if act.whatsThis()],
                key=lambda act: int(act.whatsThis()),
                reverse=True
        ):
            self.toolbar.addAction(action)
            self.toolbar.addSeparator()

        self.addToolBar(self.toolbar)

    def _setup(self, ps_tbl_cmbox_qmodels, ps_tbl_header, pmc_tbl_qmodel):
        self.setupUi(self)
        self._add_toolbar()
        self._add_logo()
        self.context_menu.addAction(self.export_pmc_act)

        self.param_selection_twidget.setRowCount(self.PS_TABLE_ROW_COUNT)
        self.param_selection_twidget.setColumnCount(len(ps_tbl_header))
        self.param_selection_twidget.setHorizontalHeaderLabels([name for name, _ in ps_tbl_header])

        for colx, cmbox_model in enumerate(ps_tbl_cmbox_qmodels):
            cmbox = QComboBox()
            cmbox.setWhatsThis(ps_tbl_header[colx][self.FK_NAME_INDEX])
            cmbox.setModel(cmbox_model)
            cmbox.setModelColumn(1)
            self.param_selection_twidget.setCellWidget(self.CMBOX_ROWX, colx, cmbox)

            cbox = QTableWidgetItem()
            cbox.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
            cbox.setCheckState(Qt.Checked)
            self.param_selection_twidget.setItem(self.CBOX_ROWX, colx, cbox)

        self.pmc_tview.setModel(pmc_tbl_qmodel)
        for colx in self.PMC_HIDDEN_COLX.values():
            self.pmc_tview.hideColumn(colx)
        self.pmc_tview.resizeColumnsToContents()

    @property
    def selected_cmboxes(self):
        for colx in range(self.param_selection_twidget.columnCount()):
            item: QTableWidgetItem = self.param_selection_twidget.item(self.CBOX_ROWX, colx)
            if item.checkState() == Qt.Checked:
                yield self.param_selection_twidget.cellWidget(self.CMBOX_ROWX, colx)

    @property
    def proj_info(self):
        return {
            self.proj_info_twidget.item(rowx, 0).whatsThis(): self.proj_info_twidget.item(rowx, 1).text()
            for rowx in range(self.proj_info_twidget.rowCount())
        }

    def dump_cfg(self):
        cfg['proj_info'] = self.proj_info

    def load_cfg(self):
        proj_info = cfg.get('proj_info')
        if not proj_info:
            return

        for rowx in range(self.proj_info_twidget.rowCount()):
            self.proj_info_twidget.item(rowx, 1).setText(
                proj_info[self.proj_info_twidget.item(rowx, 0).whatsThis()]
            )

    def contextMenuEvent(self, event):
        if self.pmc_tview.selectedIndexes() and self.pmc_tview.hasFocus():
            self.context_menu.exec_(self.mapToGlobal(event.pos()))

    def closeEvent(self, *args, **kwargs):
        self.dump_cfg()
        super().closeEvent(*args, **kwargs)
