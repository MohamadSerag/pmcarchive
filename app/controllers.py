import operator
from functools import reduce
from os.path import abspath, expandvars
from tempfile import TemporaryDirectory

from PyQt5.QtWidgets import QFileDialog, QMessageBox
from openpyxl import load_workbook

from models import *
from views import MainView
from utils import cfg


class MainCtrl:
    EMPTY_PMC_TBL_CLAUSE = '{}.id = 0'.format(PMC._meta.table_name)

    def __init__(self, view: MainView, pmc_tbl_qmodel):
        self._view = view
        self._pmc_tbl_qmodel = pmc_tbl_qmodel

        self._view.pmc_tview.setModel(self._pmc_tbl_qmodel)
        self._view.export_pmc_act.triggered.connect(self.export_pmc_file)
        self._view.search_act.triggered.connect(self.update_pmc_tbl)
        self._view.about_act.triggered.connect(lambda: QMessageBox.information(self._view, "PMC Archive", cfg['about']))

    @property
    def _selected_params(self):
        return [
            [cmbox.whatsThis(), cmbox.model().record(cmbox.currentIndex()).value('id')]
            for cmbox in self._view.selected_cmboxes
        ]

    @staticmethod
    def _make_wb(bytefile):
        with TemporaryDirectory() as tempdir:
            with open(tempdir+'\\f.xlsm', 'wb') as of:
                of.write(bytefile)
            return load_workbook(tempdir+'\\f.xlsm', keep_vba=True)

    @staticmethod
    def _write_pmc_header(wb, header, wsx=0):
        form_ws = wb.worksheets[wsx]
        for cell, value in header.items():
            form_ws[cell] = value
        return wb

    @staticmethod
    def _make_clause(attr, oper, values):
        if oper == 'IN':
            right_hand_operand = "({})".format(", ".join([str(val) for val in values]))
        return " ".join([attr, oper, right_hand_operand])

    def update_pmc_tbl(self):
        clauses = [(getattr(PMC, name) == value) for name, value in self._selected_params]
        pmcs = PMC.select().where(reduce(operator.and_, clauses)).execute()

        if not pmcs:
            QMessageBox.information(self._view, 'INFO', 'No PMC matching selected parameters!')
            self._pmc_tbl_qmodel.setFilter(self.EMPTY_PMC_TBL_CLAUSE)
        else:
            self._pmc_tbl_qmodel.setFilter(self._make_clause('{}.id'.format(PMC._meta.table_name), 'IN', [pmc.id for pmc in pmcs]))
            self._view.pmc_tview.resizeColumnsToContents()

    def export_pmc_file(self):
        path = QFileDialog.getSaveFileName(self._view, directory=expandvars('%userprofile%')+'\\Desktop', caption="New File", filter="Excel (*.xlsm)")[0]
        if not path:
            return
        selected_rowx = self._view.pmc_tview.selectedIndexes()[0].row()
        record = self._view.pmc_tview.model().record(selected_rowx)
        file = PMCFile.select().where(PMCFile.id == record.value('file_id')).get()
        wb = self._write_pmc_header(self._make_wb(file.val), self._view.proj_info)
        wb.save(path)
        QMessageBox.information(self._view, 'DONE', '<a href="file:///{}">{}</a>'.format(abspath(path), abspath(path)))
