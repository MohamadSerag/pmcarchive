import json
import os
from collections import ChainMap
from contextlib import suppress
from pathlib import Path


def get_orm_classes(module, ps_cmbox_only=False):
    from inspect import getmembers, isclass

    for name, cls in getmembers(module, isclass):
        if issubclass(cls, module._BaseModel) and '_' not in name:
            if not ps_cmbox_only:
                yield cls
            elif cls.is_ps_tbl:
                yield cls


class _Config(ChainMap):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._user_cfg_path = None
        self._app_cfg_path = None

    def init(self, app_name, app_cfg_path='res/app.cfg'):
        self._user_cfg_path = Path(os.environ.get('APPDATA', '/private/tmp'), app_name, 'user.cfg')
        self._app_cfg_path = Path(app_cfg_path)
        self._load_file(self._app_cfg_path, at_end=True)

    def _load_file(self, path, at_end=False):
        """
        I want to insert user configs at first position, so they override others and
        gets saved to user's appdata.
        ui's dump_cfg doesn't create a new map, it only updates the one in first position
        """
        with suppress(FileNotFoundError):
            with path.open() as file:
                index = len(self.maps) if at_end else 0
                self.maps.insert(index, json.load(file))
                return True

    def handle_views(self, views):
        if self._load_file(self._user_cfg_path):
            # in this case, there're 2 filled maps (user/app) and one empty in th middle
            for view in views:
                view.load_cfg()
        else:
            # this will update the first map, which in this case is initially empty
            for view in views:
                view.dump_cfg()

    def save(self, mapx=0):
        if not self._user_cfg_path.parent.exists():
            os.mkdir(str(self._user_cfg_path.parent))

        with self._user_cfg_path.open(mode='w') as f:
            json.dump(self.maps[mapx], f, indent=2)


cfg = _Config()

