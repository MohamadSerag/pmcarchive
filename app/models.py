from PyQt5.QtCore import Qt
from peewee import Proxy, Model, CharField, IntegerField, ForeignKeyField, FloatField, BlobField
from PyQt5.QtSql import QSqlQueryModel, QSqlRelationalTableModel, QSqlRelation
from PyQt5.QtSql import QSqlDatabase


qt_db_conn = None


def open_db_qconn(conn_str, driver="QSQLITE"):  # conn_str = filepath, for sqlite db
    conn = QSqlDatabase.addDatabase(driver)
    conn.setDatabaseName(conn_str)

    if not conn.open():
        print("Couldn't connect to the db!")
    else:
        return conn


def make_cmbox_model(tbl_model):
    model = QSqlQueryModel()
    stm, params = tbl_model.select().sql()
    model.setQuery(stm.replace('?', '{}').format(*params))
    return model


def make_relational_model(src_tbl_name, relations=None, clause=None):    # ref_tbl_name, fk_colx=3, fk_col_name='id', target_col_name='name', fk_col_header_label=None, clause=None):
    model = CustomRelTblModel()
    model.setTable(src_tbl_name)

    for rel in relations:
        model.setRelation(rel['fk_colx'], QSqlRelation(rel['tbl_name'], rel['fk_col_name'], rel['target_col_name']))
        if rel.get('fk_col_header_label'):
            model.setHeaderData(rel['fk_colx'], Qt.Horizontal, rel['fk_col_header_label'])

    if clause:
        model.setFilter(clause)
    model.select()
    return model


class CustomRelTblModel(QSqlRelationalTableModel):
    def data(self, QModelIndex, role=None):
        if role == Qt.TextAlignmentRole:
            return Qt.AlignCenter
        return super().data(QModelIndex, role)

#*****************************************************************************************************************************************************************************************#


orm_db_conn = Proxy()


class _BaseModel(Model):
    is_ps_tbl = False

    class Meta:
        database = orm_db_conn


class Rating(_BaseModel):
    val = IntegerField()
    is_ps_tbl = True
    display_order = 1
    fk_name = 'rating_id'

    class Meta:
        table_name = 'Ratings'


class Facing(_BaseModel):
    val = CharField()
    is_ps_tbl = True
    display_order = 2
    fk_name = 'facing_id'

    class Meta:
        table_name = 'Facings'


class Mat(_BaseModel):
    val = CharField()
    is_ps_tbl = True
    display_order = 3
    fk_name = 'mat_id'

    class Meta:
        table_name = 'Materials'


class VTrim(_BaseModel):
    val = CharField()
    is_ps_tbl = True
    display_order = 5
    fk_name = 'vtrim_id'

    class Meta:
        table_name = 'VTrims'


class Company(_BaseModel):
    name = CharField()

    class Meta:
        table_name = 'Companies'


class PMCFile(_BaseModel):
    val = BlobField()

    class Meta:
        table_name = 'PMCFiles'


class PMC(_BaseModel):
    name = CharField()

    rating = ForeignKeyField(Rating, null=True)
    facing = ForeignKeyField(Facing, null=True)
    mat = ForeignKeyField(Mat, null=True)
    vtrim = ForeignKeyField(VTrim, null=True)

    company = ForeignKeyField(Company, null=True)
    file = ForeignKeyField(PMCFile, null=True)
    ca = CharField(null=True)
    service = CharField(null=True)
    joint_construction = CharField(null=True)
    special_requirements = CharField(null=True)

    class Meta:
        table_name = 'PMCs'
