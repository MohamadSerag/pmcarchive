INSERT INTO Ratings VALUES (1,125);
INSERT INTO Ratings VALUES (2,150);
INSERT INTO Ratings VALUES (3,300);
INSERT INTO Ratings VALUES (4,600);
INSERT INTO Ratings VALUES (5,900);
INSERT INTO Ratings VALUES (6,1500);
INSERT INTO Ratings VALUES (7,2500);

INSERT INTO Facings VALUES (1,"RF");
INSERT INTO Facings VALUES (2,"FF");
INSERT INTO Facings VALUES (3,"RTJ");

INSERT INTO Materials VALUES (1,"Carbon Steel");
INSERT INTO Materials VALUES (2,"Stainless Steel");
INSERT INTO Materials VALUES (3,"Duplex Steel");
INSERT INTO Materials VALUES (4,"Super Duplex Steel");

INSERT INTO Vtrims VALUES (1,1);
INSERT INTO Vtrims VALUES (2,2);

INSERT INTO Companies VALUES (1,"Enppi");
INSERT INTO Companies VALUES (2,"Shell");

INSERT INTO ParamGroups VALUES (1,2,1,1,1);
INSERT INTO ParamGroups VALUES (2,3,1,1,1);
INSERT INTO ParamGroups VALUES (3,4,1,1,1);

INSERT INTO PMCs VALUES (1,"A3",2,1,1,1,1,null,3,"Process","SW/BW & FLG",null);
INSERT INTO PMCs VALUES (2,"B3",3,1,1,1,1,null,1.6,"Process","SW/BW & FLG",null);
INSERT INTO PMCs VALUES (3,"D3",4,1,1,1,1,null,3.2,"Process","SW/BW & FLG",null);
